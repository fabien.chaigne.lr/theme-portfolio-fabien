<?php
/*
Template Name: contact
*/
?>
<?php get_header(); ?>
<!-- body start -->
	<div id="main-contact">
        <div class="body">
            <!-- formulaire -->
            <section class="block-contact">
                <div class="image"><img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/portrait.jpg" alt="" /></div>
                <div class="infos">		
					<?php if(have_posts()) : while (have_posts()) : the_post(); ?>
           			<?php the_content(); ?>
           			<?php endwhile; else : ?>
           			<p>Bonjour, désolé, mais la page désirée n'est pas accessible.</p>
           			<?php endif; ?>
                </div>
    <form method="post">
        <div>
        <input type="text" name="nom" required value="Nom">
        </div>
		<div>
        <input type="email" name="email" required value="Mail">
        </div>
		<div>
        <textarea name="message" required>Votre message</textarea>
		</div>
		<div>
        <input class="button" type="submit">
		</div>
    
    <?php
    if(isset($_POST['message'])){
        $entete  = 'MIME-Version: 1.0' . "\r\n";
        $entete .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $entete .= 'From: ' . $_POST['email'] . "\r\n";

        $message = '<h1>Message envoyé depuis la page Contact de fabienchaigne.fr</h1>
        <p><b>Nom : </b>' . $_POST['nom'] . '<br>
        <b>Email : </b>' . $_POST['email'] . '<br>
        <b>Message : </b>' . $_POST['message'] . '</p>';

        $retour = mail('contact@fabienchaigne.fr', 'Envoi depuis page Contact', $message, $entete);
        if($retour) {
            echo '<p>Votre message a bien été envoyé.</p>';
        }
    }
    ?>
		</form>
            </section>
            <!-- body end -->
        </div>
<?php get_footer(); ?>