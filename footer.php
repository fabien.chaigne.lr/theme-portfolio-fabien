<footer class="footer">
	<a href="/sitemap/">Sitemap</a>
    <a href="/mentions-legales/">Mentions légales</a>
	<a href="https://www.behance.net/fabienchaigne" target="_blank">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/Behance.jpg" alt="behance">
	</a>
	<a href="https://www.pinterest.fr/fabienchaigne" target="_blank">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/Pinterest.jpg" alt="pinterest">
	</a>
</footer>
</div>
<script>
    const btn = document.querySelector('button');
    const nav = document.querySelector('.nav');


    btn.addEventListener('click', function() {
        nav.classList.toggle('open');
    })
</script>
<script>
function activeCompetencesM() {
  document.getElementById("competencesM").style.backgroundImage ="url('<?php echo get_template_directory_uri(); ?>/assets/Motion.gif')";
}
</script>
<script>
function activeCompetencesB() {
  document.getElementById("competencesB").style.backgroundImage ="url('<?php echo get_template_directory_uri(); ?>/assets/Branding.gif')";
}
</script>
<script>
function activeCompetencesG() {
  document.getElementById("competencesG").style.backgroundImage ="url('<?php echo get_template_directory_uri(); ?>/assets/Graphisme.gif')";
}
</script>
<script>
function activeCompetencesI() {
  document.getElementById("competencesI").style.backgroundImage ="url('<?php echo get_template_directory_uri(); ?>/assets/Illustration.gif')";
}
</script>
<script>
function activeCompetencesIn() {
  document.getElementById("competencesIn").style.backgroundImage ="url('<?php echo get_template_directory_uri(); ?>/assets/Interaction.gif')";
}
</script>
<?php wp_footer(); ?>
</body>

</html>