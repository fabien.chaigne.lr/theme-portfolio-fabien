<?php get_header(); ?>
<div id="main">
<!-- body start -->
        <div class="body">
            <!-- catégories articles -->
		
			<?php dynamic_sidebar("categories"); ?>
			
			<!-- mosaïque articles -->
            <section class="mosaic">
				<?php if(have_posts()) : while (have_posts()) : the_post(); ?>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail("liste_articles"); ?>
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <?php the_title(); ?>
                        </h2>
                        <p class="article-text-mosaic"><?php echo substr(strip_tags($post->post_content), 0, 140); ?>...</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date"><?php the_time('j F Y'); ?></div>
                            </a>
                            <a href="<?php the_permalink(); ?>" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
				 <?php endwhile; else : ?>
         <p>Bonjour, Désolé mais il n'y a aucun article sur cette page</p>
         <?php endif; ?>
             </section>
			<?php if(function_exists('wp_simple_pagination')) {
    		wp_simple_pagination();
}			?> 
            <!-- body end -->
        </div>
<?php get_footer(); ?>