<?php get_header(); ?>
<div id="main">
<!-- body start -->
        <div class="body-articles">
            <!-- article -->
            <section class="articles">
                <article class="articles">
                    <!-- photo de couverture -->
                    <div class="background-img">
						<?php the_post_thumbnail(); ?>
                    </div>
                    <!-- titre -->
                    <div class="articles-titre">
                        <h1> <?php the_title(); ?> </h1>
                    </div>
                    <!-- contenu -->
                    <div class="contenu-articles">
                        <?php the_content(); ?>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                    <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date"><?php the_time('j F Y'); ?></div>
                            </a>
                        </div>
                    </div>
                </article>
            </section>
			<?php the_posts_pagination(); ?>
            <!-- body end -->
        </div>

<?php get_footer(); ?>