<?php get_header(); ?>
<div id="main">
<!-- body start -->
        <div class="body">
            <!-- vidéo -->
            <section>
                <div class="block-video-gradient">
					<div class="block-video-img">
						<a href="Vidéo-motion-design" class="block-video-bouton">
							<h1>BIENTÔT LA VIDÉO</h1>
						</a>
					</div>					
                </div>
            </section>
            <!-- compétences -->
            <section class="skills">
                <div class="block-competences-r">
                    <h1 class="titre-competences-r">Motion</h1>
                </div>
                <div id="competencesM" onclick="activeCompetencesM()" class="img-competences-motion">
					<p class="p-competences">Motion</p>
                </div>
                <div class="block-competences-l">
                    <h1 class="titre-competences-l">Branding</h1>
                </div>
                <div id="competencesB" onclick="activeCompetencesB()" class="img-competences-branding">
					<p class="p-competences">Branding</p>
                </div>
                <div class="block-center">
					
                    <?php if(have_posts()) : while (have_posts()) : the_post(); ?>
           			<?php the_content(); ?>
           			<?php endwhile; else : ?>
           			<p>Bonjour, désolé, mais la page désirée n'est pas accessible.</p>
           			<?php endif; ?>
                </div>
                <div class="block-competences-r">
                    <h1 class="titre-competences-r">Graphisme</h1>
                </div>
                <div id="competencesG" onclick="activeCompetencesG()" class="img-competences-graphisme">
					<p class="p-competences">Graphisme</p>
                </div>
                <div class="block-competences-l">
                    <h1 class="titre-competences-l">Illustration</h1>
                </div>
                <div id="competencesI" onclick="activeCompetencesI()" class="img-competences-illustration">
					<p class="p-competences">Illustration</p>
                </div>
                <div class="block-competences-r">
                    <h1 class="titre-competences-r">Web</h1>
                </div>
                <div id="competencesIn" onclick="activeCompetencesIn()" class="img-competences-interaction">
					<p class="p-competences">Web</p>
                </div>
            </section>
            <!-- à la une -->
            <section class="une">
				<div class="sous-block-projets">
                        <h1>NOUVEAUX PROJETS</h1>
				</div>
                <article class="article-1">
				<?php query_posts('order=desc&showposts=4'); ?>
                <?php while (have_posts()) : the_post(); ?>
                    <figure class="article-figure-accueil">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail("liste_articles"); ?>
                        </a>
                    </figure>

                    <div class="article-content">
                        <h2 class="article-title">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h2>
                        <p class="article-text"><?php echo substr(strip_tags($post->post_content), 0, 100); ?>...<br /></p>
                        <a href="<?php the_permalink(); ?>" class="article-btn">lire la suite</a>
                    </div>
				<?php endwhile; ?>
               	<?php wp_reset_query(); ?>
                </article>
            </section>
            <!-- body end -->
        </div>
        <!-- footer -->
        
<?php get_footer(); ?>