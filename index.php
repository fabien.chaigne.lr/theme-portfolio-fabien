<?php get_header(); ?>
<div id="main">
<!-- body start -->
        <div class="body">
            <!-- mosaïque articles -->
            <section class="etiquette">
                <p>étiquette 1</p>
                <p>étiquette 2</p>
                <p>étiquette 3</p>
                <p>étiquette 4</p>
                <p>étiquette 5</p>
                <p>étiquette 6</p>
            </section>
            <section class="mosaic">
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic"><span class="20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus consequatur exercitationem, rem aut fugit itaque, accusamus ab iste aspernatur nobis ea quod fuga sit hic natus quae inventore aliquam autem.</span></p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic"><span class="15">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque eligendi molestiae, quibusdam voluptatem praesentium et, suscipit dicta hic illum dolore non nobis est, ratione nesciunt autem laboriosam corporis nihil ex.</span></p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
                <article class="article">
                    <figure class="article-figure-mosaic">
                        <a href="#">
                            <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/Art-board.jpg" alt="">
                        </a>
                    </figure>
                    <div class="article-mosaic">
                        <h2 class="article-title-mosaic">
                            <a href="#">Titre de l'article</a>
                        </h2>
                        <p class="article-text-mosaic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam at excepturi odit dolores sed consequuntur.</p>
                        <div class="article-top">
                            <a href="#">
                                <div class="article-date">
                                <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/picto-date.svg" alt="Date">Date</div>
                            </a>
                            <a href="#" class="article-btn-mosaic">lire la suite</a>
                        </div>
                    </div>
                </article>
            </section>
            <!-- body end -->
        </div>
<?php get_footer(); ?>