<?php
/*
Template Name: prestations
*/
?>
<?php get_header(); ?>
<div id="main">
<!-- body start -->
        <div class="body">
            <!-- liste prestations -->
            <div class="top-body">
				<h1>LE PROCESS</h1>
            </div>
			<div class="workflow">
				<div class="workflow-boite">
					<img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/projet.svg" alt="">
					<h1>Projet</h1>
					<p>Donnez vie à vos idées</p>
				</div>
				<div class="workflow-boite">
					<img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/contact.svg" alt="">
					<h1>Contact</h1>
					<p>Ensemble définissons les contours de votre projet</p>
				</div>
				<div class="workflow-boite">
					<img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/devis.svg" alt="">
					<h1>Devis</h1>
					<p>Estimez la valeur de vos attentes</p>
				</div>
				<div class="workflow-boite">
					<img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/creation.svg" alt="">
					<h1>Création</h1>
					<p>Concevons les moyens qui feront de votre projet un succès</p>
				</div>
				<div class="workflow-boite">
					<img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/validation.svg" alt="">
					<h1>Validation</h1>
					<p>Livraison des éléments conforment à vos attentes</p>
				</div>
			</div>
            <div class="top-body">
				<h1>LES PRESTATIONS</h1>
            </div>
            <section class="block-prestation">
				<?php query_posts('post_type=prestation');
				while (have_posts()) : the_post();
				//Récupérer les catégories de chaque projet
				$terms = get_the_terms($post->ID, 'categories');
				$terms_name = array();
				foreach($terms as $term) { $terms_name[] = $term->name; }
				?>
                <article class="prestation" >
                    <figure class="prestation-figure">
                            <?php the_post_thumbnail('presta'); ?>
                    </figure>
                    <div class="prestation-article">
                        <h2 class="prestation-article-title">
                            <?php the_title(); ?>
                        </h2>
                        
						<?php the_content(); ?>
						
                    </div>
                </article>
                <div class="bottom-prestation">
                </div>
                <?php endwhile; ?>
        <?php wp_reset_query(); ?>
            </section>
            <!-- body end -->
        </div>
<?php get_footer(); ?>