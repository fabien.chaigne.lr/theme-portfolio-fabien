<?php

//==========================================================
//===================         chargement des scripts
//==========================================================
define('fcpor_VERSION', '1.0.0');

// chargement dans le front-end
function fcpor_scripts() {

// chargements des styles
//wp_enqueue_style( 'fcpor_custom', get_template_directory_uri() . '/style.css', array(), fcpor_VERSION, 'all' );

// chargement des scripts
//wp_enqueue_script( 'fcpor_script', get_template_directory_uri() . '/js/themefcnav.js', array(), fcpor_VERSION, true );
// wp_enqueue_script( 'fcpor_script', get_template_directory_uri() . '/js/themefc.js', array(jquery), fcpor_VERSION, true );

} // fin function fcfab_scripts

add_action('wp_enqueue_scripts', 'fcpor_scripts');

//================================================
//======        Utilitaires
//===============================================
function fcpor_setup() {
   
   	//support des vignettes
	add_theme_support('post-thumbnails');
	add_image_size('liste_articles', "auto", 300, true);
	add_image_size('presta', "auto", 300 , true);

	//enlève génération de version
	remove_action('wp_head','wp_generator');

	//enlève les guillemets à la française
	//remove_filter('the_content','wptexturize');

	//support du titre
	add_theme_support( 'title-tag' );
	
	//active la gestion de menu
	add_theme_support( 'nav_menu' );
	//active les nouveaux menus du thème
	register_nav_menus( array( 'primary' => 'principal'));
	//primary = identifiant interne et principal = libellé back-end

 } // fin function fcfab_setup

add_action('after_setup_theme', 'fcpor_setup');

// categories
if (function_exists('register_sidebar'))
register_sidebar(array('name'=>'categories',
'before_widget'=>'<section class="etiquette">',
'after_widget'=>'</section>',
'before_title'=>'<h1>',
'after_title'=>'</h1>',
));

// Custom post type - Mes prestations
add_action('init', 'prestation_init');
function prestation_init()
{
	$labels = array(
	'name' => 'Prestations',
	'singular_name' => 'Prestation',
	'add_new' => 'Ajouter une prestation',
	'add_new_item' => 'Ajouter une nouvelle prestation',
	'edit_item' => 'Modifier une prestation',
	'new_item' => 'Nouvelle prestation',
	'view_item' => 'Voir la prestation',
	'search_items' => 'Rechercher une prestation',
	'not_found' => 'Aucune prestations trouvé',
	'not_found_in_trash' => 'Aucune prestations dans la corbeille',
	'menu_name' => 'Mes prestations'
	
	);
	
	$args = array(
	'labels' => $labels,
	'public' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'menu_position' => 2,
	'supports' => array('title','editor','thumbnail','custom-fields','comments')
	
	);
	register_post_type('prestation',$args);
}

add_action ('init', 'create_taxonomy', 0);
function create_taxonomy()
{
	$labels = array(
	'name' => 'Catégories prestations',
	'singular_name' => 'Catégorie prestation',
	'search_items' => 'Rechercher une catégorie',
	'all_items' => 'Toutes les catégories',
	'edit_item' => 'Editer une catégorie',
	'update_item' => 'Modifier une catégorie',
	'add_new_item' => 'Ajouter une catégorie',
	'new_item_name' => 'Nouvelle catégorie',
	'menu_name' => 'Catégorie'
	
	);
	
	register_taxonomy('categories', array('prestation'), array (
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array('slug' => 'categories')
	
	
	));
}

// Afficher image supplémentaire pour le porfolio

if( class_exists( 'kdMultipleFeaturedImages' ) ) {

        $args = array(
                'id' => 'folio-image-1',
                'post_type' => 'portfolio',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Image N°1',
                    'set'       => 'Ajouter image',
                    'remove'    => 'Supprimer image',
                    'use'       => 'Utiliser image',
                )
        );

        new kdMultipleFeaturedImages( $args );
}

if( class_exists( 'kdMultipleFeaturedImages' ) ) {

        $args = array(
                'id' => 'folio-image-2',
                'post_type' => 'portfolio',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Image N°2',
                    'set'       => 'Ajouter image',
                    'remove'    => 'Supprimer image',
                    'use'       => 'Utiliser image',
                )
        );

        new kdMultipleFeaturedImages( $args );
}


if( class_exists( 'kdMultipleFeaturedImages' ) ) {

        $args = array(
                'id' => 'folio-image-3',
                'post_type' => 'portfolio',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Image N°3',
                    'set'       => 'Ajouter image',
                    'remove'    => 'Supprimer image',
                    'use'       => 'Utiliser image',
                )
        );

        new kdMultipleFeaturedImages( $args );
}

// logo client
if( class_exists( 'kdMultipleFeaturedImages' ) ) {

        $args = array(
                'id' => 'image-client',
                'post_type' => 'portfolio',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Logo client',
                    'set'       => 'Ajouter logo',
                    'remove'    => 'Supprimer logo',
                    'use'       => 'Utiliser logo',
                )
        );

        new kdMultipleFeaturedImages( $args );
}


// affichage pour le portfolio
function iti_custom_posts_per_page($query)
{
	switch ($query->query_vars['post_type'])
	{
		case 'portfolio': // Nom du post type
		$query->query_vars['posts_per_page'] = 10;
		break;
	}
	return $query;
}

if(!is_admin())
{
	add_filter('pre_get_posts','iti_custom_posts_per_page');
}

?>