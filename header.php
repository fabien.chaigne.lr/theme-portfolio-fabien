<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.typekit.net/yuj8kzd.css">
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css">
	<script data-ad-client="ca-pub-7977338689306015" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <?php wp_head(); ?>
</head>

<body>
            <!-- header -->
        <header class="header">
            <div class="header-top">

                <div class="logo">
                    <a href="/">
                        <img class="logo-jpg" src="<?php bloginfo( 'stylesheet_directory' );?>/assets/logo.jpg" alt="logo">
                        <img class="titre-logo" src="<?php bloginfo( 'stylesheet_directory' );?>/assets/titre-logo.svg" alt="fabien chaigne graphiste motion designer">
                    </a>
                </div>

                <div class="btn-menu">
                    <button>
                        <img src="<?php bloginfo( 'stylesheet_directory' );?>/assets/burger-menu.svg" alt="">
                    </button>
                </div>

            </div>
            <nav class="nav">
                
                   <?php wp_nav_menu(); ?>
               
            </nav>
        </header>